<h1>Post Importer</h1>
<div class="option-block">
    <h3>Feed URL</h3>
    <input type="text" name="feedurl" value="">
</div>
<div class="option-block">
    <h3>Category Rules</h3>
    <ul id="rule-list">
        <li id="1">
            <div>If post
                <select name="rules">
                    <option value="words">has words</option>
                    <option value="nowords">has NOT words</option>
                    <option value="phrase">has exact phrase</option>
                    <option value="nophrase">has NOT exact phrase</option>
                </select>
                <input type="text" name="tomatch">
                add category
                <select name="categories">
					<?php
					$args = array(
						'orderby' => 'id',
						'order' => 'DESC',
						'hide_empty'=> 0,
					);
					$categories = get_categories($args);
					foreach ($categories as $category) {
						echo '<option>' . $category->name . '</option>';
					}
					?>
                </select>
                <button type="button" onclick="deleteRule(this.parentElement.parentElement.id)">Delete</button>
            </div>
        </li>
    </ul>
    <button type="button" onclick="addRule()">Add rule</button>
</div>
<div class="option-block">
    <h3>Default Category</h3>
    <div>If none of the rules apply, default category should be<select name="categories">
			<?php
			$categories = get_categories($args);
			foreach ($categories as $category) {
				echo '<option>' . $category->name . '</option>';
			}
			?>
        </select>
    </div>
</div>
<div class="option-block">
    <h3>Time interval</h3>
    New posts will be fetch
    <select name="interval">
        <option value="hourly">Hour</option>
        <option value="daily">Day</option>
        <option value="weekly">Week</option>
        <option value="monthly">Month</option>
    </select>
</div>
<input type="submit" name="Save options">
</form>