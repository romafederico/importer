<?php

class Post_Importer_Admin {

	private $plugin_name;
	private $version;

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	public function plugin_add_admin_menu(  ) {
		add_submenu_page( 'options-general.php', 'Post Importer', 'Post Importer', 'manage_options', 'post_importer', array($this, 'post_importer_options_page'));
	}

	public function register_settings() {
	    for ($i = 1; $i <= 10; $i++) {
		    register_setting( 'post_importer_settings', 'tomatch'.$i );
		    register_setting( 'post_importer_settings', 'categories'.$i );
		    register_setting( 'post_importer_settings', 'language'.$i );
        }
		register_setting( 'post_importer_settings', 'feedurl' );
		register_setting( 'post_importer_settings', 'status' );
		register_setting( 'post_importer_settings', 'defaultcat' );
		register_setting( 'post_importer_settings', 'defaultlang' );
		register_setting( 'post_importer_settings', 'interval' );
	}

	public function post_importer_options_page() {
		?>
            <div class="wrap">
                <h1>Post Importer</h1>

                <!--CRAWL NOW SECTION-->
                <div class="option-block">
                    <h3>Click to check if there is new posts.</h3>
                    <form action="options-general.php?page=post_importer" method="post" name="button">
		                <?php wp_nonce_field('parse_button_clicked'); ?>
                        <input type="hidden" value="true" name="test_button" />
		                <?php submit_button('Crawl now!'); ?>
                    </form>
	                <?php

	                if (isset($_POST['test_button']) && check_admin_referer('parse_button_clicked')) {
		                $this->fetchPosts();
	                }
	                ?>
                </div>

                <!--CRON JOB SECTION-->
                <div class="option-block">
                    <h3>Time interval</h3>
                    <form action="options-general.php?page=post_importer" method="post" name="button">
	                    <?php wp_nonce_field('cron_button_clicked'); ?>
                        <input type="hidden" value="true" name="cron_button" />
                        New posts will be fetched every
                        <select name="interval">
                            <option value="">&mdash; None &mdash;</option>
                            <option value="five" <?php echo get_option('interval') == 'five' ? 'selected="selected"' : ''; ?>>Five minutes</option>
                            <option value="hour" <?php echo get_option('interval') == 'hour' ? 'selected="selected"' : ''; ?>>Hour</option>
                            <option value="day" <?php echo get_option('interval') == 'day' ? 'selected="selected"' : ''; ?>>Day</option>
                            <option value="week" <?php echo get_option('interval') == 'week' ? 'selected="selected"' : ''; ?>>Week</option>
                            <option value="month" <?php echo get_option('interval') == 'month' ? 'selected="selected"' : ''; ?>>Month</option>
                        </select>
	                    <?php submit_button('Update time interval'); ?>
                    </form>
	                <?php

	                if (isset($_POST['cron_button']) && check_admin_referer('cron_button_clicked')) {
		                $this->change_cron_interval($_POST['interval']);
	                }
	                ?>
                </div>

                <!--RULES SECTION-->
                <form action="options.php" method="post" name="form">
                    <?php
                        settings_fields( 'post_importer_settings' );
                        do_settings_sections( 'post_importer_settings' );
                    ?>
                    <div class="option-block">
                        <h3>Feed URL</h3>
                        <input type="text" name="feedurl" value="<?php echo esc_attr(get_option('feedurl')); ?>" size="100">
                    </div>

                    <!--DEFAULT STATUS SECTION-->
                    <div class="option-block">
                        <h3>Default Posts Status</h3>
                        <div>Posts status should be imported as
                            <select name="status">
                                <option value="">&mdash; None &mdash;</option>
                                <option value="draft" <?php echo get_option('status') == 'draft' ? 'selected="selected"' : ''; ?>>Draft</option>
                                <option value="publish" <?php echo get_option('status') == 'publish' ? 'selected="selected"' : ''; ?>>Published</option>
                            </select>
                        </div>
                    </div>

                    <div id="crawl-result"></div>

                    <!--RULES SECTION-->
                    <div class="option-block">
                        <h3>Category Rules</h3>
                        <p>Wrap words between quotes ("") to search for exact phrases</p>
                        <ul id="rule-list">
	                        <?php

	                        for ($i = 1; $i <= 10; $i++) {
		                        ?>
                                <li id="<?php echo $i; ?>>">
                                    <div>
                                        If post has any of the words
                                        <input type="text" name="tomatch<?php echo $i; ?>" value="<?php echo esc_attr(get_option('tomatch'.$i)); ?>" size="30">
                                        add category
                                        <select name="categories<?php echo $i; ?>">
                                            <option value="">&mdash; None &mdash;</option>
					                        <?php
					                        $args = array(
						                        'orderby' => 'id',
						                        'order' => 'DESC',
						                        'hide_empty'=> 0,
					                        );
					                        $categories = get_categories($args);

					                        foreach ($categories as $category) {

						                        if(ICL_LANGUAGE_CODE == 'de') {
							                        $option = apply_filters( 'wpml_object_id', get_option('categories'.$i), 'category', true, 'de');
						                        } else {
							                        $option = apply_filters( 'wpml_object_id', get_option('categories'.$i), 'category', true, 'en');
						                        }

						                        ?>
                                                <option value="<?php echo $category->term_id; ?>" <?php

						                        if ($option == $category->term_id) {
							                        echo 'selected="selected"';
						                        } else {
							                        echo '';
						                        }
						                        ?>
                                                ><?php echo $category->name ?></option>
						                        <?php
					                        }


//					                        foreach ($categories as $category) {
//						                        ?>
<!--                                                <option value="--><?php //echo $category->name ?><!--" --><?php //echo get_option('categories'.$i) == $category->name ? 'selected="selected"' : ''; ?><!-->--><?php //echo $category->name ?><!--</option>-->
<!--						                        --><?php
//					                        }
					                        ?>
                                        </select>
                                        and the language should be
                                        <select name="language<?php echo $i; ?>">
                                            <option value="">&mdash; None &mdash;</option>
                                            <option value="de" <?php echo get_option('language'.$i) == 'de' ? 'selected="selected"' : ''; ?>>Deutsch</option>
                                            <option value="en" <?php echo get_option('language'.$i) == 'en' ? 'selected="selected"' : ''; ?>>English</option>
                                        </select>
                                    </div>
                                </li>
		                        <?php
	                        }

	                        ?>
                        </ul>
                    </div>

                    <!--DEFAULT RULE SECTION-->
                    <div class="option-block">
                        <h3>Default Category</h3>
	                        <div>If none of the rules apply, default category should be
                            <select name="defaultcat">
                                <option value="">&mdash; None &mdash;</option>
                                <?php
                                $args = array(
	                                'orderby' => 'id',
	                                'order' => 'DESC',
	                                'hide_empty'=> 0,
                                );
				                $categories = get_categories($args);
				                foreach ($categories as $category) {
					                ?>
<!--                                        <option value="--><?php //echo $category->name ?><!--" --><?php //echo get_option('defaultcat') == $category->name ? 'selected="selected"' : ''; ?><!-->--><?php //echo $category->name ?><!--</option>-->
                                        <option value="<?php echo $category->term_id ?>" <?php

                                        if(ICL_LANGUAGE_CODE == 'de') {
	                                        $defaultcat = apply_filters( 'wpml_object_id', get_option('defaultcat'), 'category', true, 'de');
                                        } else {
	                                        $defaultcat = apply_filters( 'wpml_object_id', get_option('defaultcat'), 'category', true, 'en');
                                        }

                                        if ($defaultcat == $category->term_id) {
	                                        echo 'selected="selected"';
                                        } else {
	                                        echo '';
                                        }

                                        ?>><?php echo $category->name ?></option>
					                <?php
				                }
				                ?>
                            </select>
                                and default language
                                <select name="defaultlang">
                                    <option value="">&mdash; None &mdash;</option>
                                    <option value="de" <?php echo get_option('defaultlang') == 'de' ? 'selected="selected"' : ''; ?>>Deutsch</option>
                                    <option value="en" <?php echo get_option('defaultlang') == 'en' ? 'selected="selected"' : ''; ?>>English</option>
                                </select>
                        </div>
                    </div>

                    <!--SUBMIT BUTTON SECTION-->
                    <div>
                        <?php
                            submit_button('Save settings');
                        ?>
                    </div>
                </form>
            </div>
		<?php

	}

	public function fetchPosts() {

//	    Get the ulr and make http request
		$url = get_option('feedurl');
		$request = new WP_Http();
		$response = $request->request( $url );

//		Convert response into string
		$body = $response['body'];

//		Get all tags <item> and store them in the $matches array
		$regex = '/<item>(.*?)<\/item>/s';
		preg_match_all($regex, $body, $matches);

		$items = $matches[0];

		foreach ($items as $item) {
            $post = array();

//				Define the tags from each section
            $arr  = array(
                "<!-- BEGIN BODY // -->",
                "<!-- // END BODY -->",
                "<title>",
                "</title>",
                "<guid>",
                "</guid>",
                "<pubDate>",
                "</pubDate>",
                "<![CDATA[",
                "]]>"
            );

//				Remove tags from all items and store them into the $post array
            $regex = '/<title>(.*?)<\/title>/s';
            preg_match( $regex, $item, $title );
            $post['title'] = str_replace( $arr, "", $title[0] );

            $regex = '/<guid>(.*?)<\/guid>/s';
            preg_match( $regex, $item, $guid );
            $post['guid'] = str_replace( $arr, "", $guid[0] );

            $regex = '#<!-- BEGIN BODY // -->(.*?)<!-- // END BODY -->#s';
            preg_match( $regex, $item, $content );
            $post['content'] = str_replace( $arr, "", $content[0] );

            $regex = '/<pubDate>(.*?)<\/pubDate>/s';
            preg_match( $regex, $item, $pubDate );
            $post['date'] = str_replace( $arr, "", $pubDate[0] );

//              for each post we pass the $post array to the function to create posts in database
            $this->createPost($post);
		}

		?>
        <div id="message" class="notice notice-success is-dismissible">Posts fetched</div>
		<?php
    }

	public function createPost($post) {

//	    create arguments to check if post guid exists
        $meta_args = array(
            'post_type'     => 'post',
            'meta_query' => array(
                array(
                    'key' => 'guid',
                    'value' => $post['guid']
                )
            )
        );

//        Making the request to database
        $query = new WP_Query($meta_args);

        if (!$query->have_posts()) {

//          If the guid doesn't exist we bring the words from title and rules, and store them into different arrays - We also remove punctuation and spaces.
	        $title = $post['title'];
	        $categories = array();
//	        Clear $lang variable for next post
            $lang = '';

	        for ($i = 1; $i <= 10; $i++) {

//	            Get the words to match as a string
	            $wtm = get_option('tomatch'.$i);

//	            Search for words between quotes and store them in $phrase
                preg_match( '/"(.*?)"/s', $wtm, $phrase);

//              Remove the $phrase from the original word string
		        $wtma = str_replace( $phrase[1], "", $wtm);

//		        Since the phrase is no longer in the word string, I remove all punctuaction and explode words into an array and remove empty spaecs from the array
		        $wtmb = explode(' ', $wtma);

//		        Push the phrase without quotes into the array as a single word
                array_push($wtmb, $phrase[1]);

                $clean_wtm = array_filter($wtmb);

//              Intersect all items of the array into the title as a string
                foreach($clean_wtm as $w) {
	                if (stripos($title, $w) !== false) {
		                ${'intersection'.$i} = true;
                        $lang = get_option('language'.$i);
	                }
                }

//  	        We check which post title intersects with the different groups of words defined in rules
                if(${'intersection'.$i}) {

	                if(get_option('language'.$i) == 'de') {
		                $cat = apply_filters( 'wpml_object_id', get_option('categories'.$i), 'category', true, 'de');
	                } else {
		                $cat = apply_filters( 'wpml_object_id', get_option('categories'.$i), 'category', true, 'en');
	                }
                    array_push($categories, $cat);
                }

            }

//          Grab string date and convert into wordpress date format
            $d = date('Y-m-d', strtotime($post['date']));

	        $args = array(
		        'post_title' => $post['title'],
                'post_status' => get_option('status'),
                'post_date' => $d,
		        'post_content' => $post['content'],
		        'guid' => $post['guid'],
	        );

//          If there is no intersections between titles and rules then we apply default category
	        if(empty($categories)) {
		        if(get_option('defaultlang') == 'de') {
			        $cat = apply_filters( 'wpml_object_id', get_option('defaultcat'), 'category', true, 'de');
		        } else {
			        $cat = apply_filters( 'wpml_object_id', get_option('defaultcat'), 'category', true, 'en');
		        }
		        array_push($categories, $cat);
            }

//            Create the post and return ID for future updates
	        $id = wp_insert_post($args);

//	        Add categories to post just created
	        wp_set_post_categories($id, $categories);

//	        Save guid as post meta
	        update_post_meta($id, 'guid', $post['guid']);

//	        If no rules apply then default language is german - 'de'
	        if(!$lang) {
	            $lang = get_option('defaultlang');
            }

	        $language_args = array(
		        'element_id' => $id,
		        'language_code' => $lang,
	        );

	        do_action( 'wpml_set_element_language_details', $language_args );
        }
    }

    public function strip_punctuation($string) {
	    $string = strtolower($string);
	    $string = preg_replace("/[^a-zA-Z 0-9]+/", "", $string);
	    $string = str_replace(" +", "", $string);
	    return $string;
    }


	public function change_cron_interval($interval) {
		wp_clear_scheduled_hook('post_importer_cron');
        wp_schedule_event(time(), $interval, 'post_importer_cron');

		?>
        <div id="message" class="notice notice-success is-dismissible">Cron job updated</div>
		<?php
    }

	public function isa_add_cron_recurrence_interval( $schedules ) {

		$schedules['five'] = array(
			'interval'  => 300,
			'display'   => __( '5 Minutes', 'textdomain' )
		);

		$schedules['hour'] = array(
			'interval'  => 3600,
			'display'   => __( 'Every Hour', 'textdomain' )
		);

		$schedules['day'] = array(
			'interval'  => 86400,
			'display'   => __( 'Every Day', 'textdomain' )
		);

		$schedules['week'] = array(
			'interval'  => 604800,
			'display'   => __( 'Every Week', 'textdomain' )
		);

		$schedules['month'] = array(
			'interval'  => 2592000,
			'display'   => __( 'Every Month', 'textdomain' )
		);

		return $schedules;
	}

	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/post-importer-admin.css', array(), $this->version, 'all' );

	}

	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/post-importer-admin.js', array( 'jquery' ), $this->version, false );

	}
}


