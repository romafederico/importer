(function( $ ) {
	'use strict';

    function addRule() {
        var ruleItem = document.getElementById('rule-list').lastElementChild;
        var ruleItemClone = ruleItem.cloneNode(true);
        ruleItemClone.setAttribute('id', parseInt(ruleItem.getAttribute('id')) + 1);
        document.getElementById('rule-list').appendChild(ruleItemClone);
    }

    function deleteRule(rule) {
        console.log(rule)
        document.getElementById(rule).remove();
    }

})( jQuery );
